const path = require('path');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
    // mode: 'production',
    devtool: 'inline-source-map',
    entry: ['idempotent-babel-polyfill', './src/index.js'],
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
        ]
    },
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],
    output: {
        filename: 'bundle.js',
        // path: path.resolve(__dirname, 'public')
        path: '/public'
    }
};

const path = require('path');
const express = require('express');

// const webpack = require('webpack');
// const webpackConfig = require('./webpack.config');
// const compiler = webpack(webpackConfig);

const app = express();
const port = 3000;

// app.use(
//     require('webpack-dev-middleware')(compiler, {
//         noInfo: true,
//         publicPath: webpackConfig.output.publicPath
//     })
// );
// app.use(require('webpack-hot-middleware')(compiler));

app.get('/', function(req, res, next) {
    let options = {
        root: path.join(__dirname, 'public'),
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };

    res.sendFile('index.html', options, function (err) {
        if (err) {
            next(err)
        } else {
            console.log('Sent: index.html');
        }
    });
});

app.use('/', express.static(path.join(__dirname, 'public')));

//

app.listen(port, () => console.log(`Give Orders listening op port: ${port}!`));

